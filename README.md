# Zoonami
Are you ready for an adventure? Zoonami adds over 50 monsters to your Minetest world that you can collect, train, and battle. Explore the world to find new monsters, villages, and resources. Battle against wild monsters, NPCs, or even your friends using an interface that looks like a real 2D RPG.

To get started, every player is given a Zoonami Guide Book. The guide book contains all the information you'll need to succeed. Players who are unfamiliar with Zoonami should start by reading the "Introduction" section. This will teach you how to choose a starter monster, heal monsters, and battle wild monsters.

[Version 1.1.0](https://codeberg.org/isaiah658/zoonami/src/branch/master/CHANGELOG.md)

## Main Features
* **Exploration** - Incentivizes players to explore the world. Monsters can be found in different places depending on the time of day, light level, and surrounding environment. Villages can help players resupply when out adventuring. Berry bushes, excavation sites, and other resources can be found throughout the world.
* **Building** - Players can build their own villages to attract NPCs.
* **Multiplayer** - Makes playing with friends even more fun. Players can battle other players, trade monsters, and create shops for other players to purchase items.
* **Documentation** - Everything players need to know is accessible in the game via the guide book, monster journal, and move journal.
* **Gameplay** - Similar to other monster catching games, but not an exact clone. Players already familiar with the genre should find new game mechanics to learn and discover.
* **Efficiency** - Mob AI is kept simple to fulfill the need for many mobs to be around players while keeping the CPU usage low.
* **Customization** - Offers many settings to tweak the gameplay experience.

## Updates
Even though Zoonami has reached an official stable release, future updates might not be compatible with existing worlds. Always read the [CHANGELOG.md](https://codeberg.org/isaiah658/zoonami/src/branch/master/CHANGELOG.md) file to verify that it is ok to update the mod. It's highly recommended to make a backup of the world before updating as well. The version numbers, such as 1.0.0, represent major, minor, and revision updates. Major updates include one or more changes that significantly alter the gameplay experience. Minor updates include smaller content updates. Revision updates include bug fixes.

## Compatibility
Zoonami is compatible with most games, mods, and mapgen. However, some of these might make progression in Zoonami difficult or impossible. Minetest the Game, MineClone 2, and other games based on these two games are the "safest" options when it comes to a balanced gameplay experience. A more detailed explanation is provided in the [COMPATIBILITY.md](https://codeberg.org/isaiah658/zoonami/src/branch/master/COMPATIBILITY.md) file.

## Requirements
* Minetest 5.4.0 or newer
* [FSC mod](https://forum.minetest.net/viewtopic.php?t=19203)

## Optional Mods
* The [Zoonami 3D Mobs mod](https://codeberg.org/isaiah658/zoonami_3d_mobs) changes the 2D monster mobs roaming the world into 3D when installed.
* The optional default and mcl_sounds mods are only to use sounds from those mods if they are installed; they provide no new content in Zoonami.

## Links
* [Zoonami Minetest Forum Topic](https://forum.minetest.net/viewtopic.php?f=9&t=25356&sid=1ffebc6a6c8b35653d939a376a067a7f)
* [Zoonami Source Code](https://codeberg.org/isaiah658/zoonami)

## Licensing
Licensing can be found in the [LICENSE.txt](https://codeberg.org/isaiah658/zoonami/src/branch/master/LICENSE.txt) file.

## Additional
I'm thankful for the skill and time God has given me to complete this project. I'm even more thankful that God has given me a path to Heaven through Jesus Christ. For anyone wondering how you can get right with God and have a path to Heaven, [GotQuestions.org](https://www.gotquestions.org/) has answers. It introduced me to the Bible and I've continued using the website for over a decade.
