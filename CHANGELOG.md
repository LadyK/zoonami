# Zoonami Changelog

## Upgrade Pathes
* Existing worlds using Zoonami 0.15.0 or higher can be upgraded to Zoonami 1.1.0

## Version 1.1.0
* 3 new monsters (bringing the total to 53)
* NPC trainer difficulty is now weighted to be more likely to match the player's average monster level
* NPC trainer monsters will now sometimes have taught moves instead of only level up moves
* Wild monster levels are weighted towards the high end of the random level range as the average level of a player's monster exceeds the range
* Slightly adjusted the stats for some of the monsters
* Berry bushes now grow a little slower
* Added a crafting recipe to make mystery move books from existing move books (3 for 1)
* NPC merchants will now sometimes sell mystery moves books
* Excavation sites now allow for liquids and torches to be above the debris nodes
* Reworded the guide book to clarify that any jelly color can be used for taming
* Berry and flower prices in automatic vending machines have been slightly lowered
* Crystal glass prices from NPC merchants have been slightly lowered
* Removed old information in the guide book about NPC merchants selling berries
* The mob spawn table was changed to add only what is needed for each monster instead of initializing everything
* Fixed being unable to delete monsters from the computer
* Fixed a crash when buying from an NPC merchant with a full inventory
* Fixed vending machine item image button names

## Version 1.0.3
* Removed test node for testing level 100 battles
* Fixed player crafted vending machines not dropping anything when broken
* Fixed a crash when trading monsters at a trading machine
* Fixed prism working on entities other than monsters, such as NPCs

## Version 1.0.2
* Fixed missing crafting recipes for candy
* Fixed old guide book information in the training and creative sections

## Version 1.0.1
* Fixed a bug with itemstacks that could result in duplicating or deleting certain items
* Reworded the guide book introduction section to make it clearer that players should read all of it to receive a few starter items

## Version 1.0.0
Major Changes
* 6 new monsters (bringing the total to 50)
* 8 new moves (bringing the total to 91)
* Overhauled how monster stats are stored to allow changes to base stats to affect all existing monsters in worlds
* Added monster personalities which boost different stats depending on the personality
* Added monster morphing
* Monsters are now categorized by tiers for PVP purposes instead of spawn rarity
* Adjusted the stats, moves, and battle rewards for monsters
* New move type (shield moves)
* Overhauled the guide book index to have labeled and color coded sections
* Added info about shield moves, blitz moves, morphing, mystery eggs, and the mob spawner to the guide book
* Adjusted sky spawn height level to be 30 for v6 mapgen, 53 for flat mapgen, and 70 for all other mapgens
* Added berry juice for healing monsters when out exploring
* Overhauled the README.md file and added a COMPATIBILITY.md file

Minor Changes
* Prisma monsters now have a small prism icon during battle to make them easier to distinguish
* The amount of ZC earned from battles now has diminishing returns for higher level monsters
* Trainer battles now give 1.5 times more ZC instead of 2.5
* The battle chat bar is now hidden during battle when a player is unable to use it
* Enemy AI was improved to avoid using unnecessary energy when opponent is in KO range
* Added a mystery move book to act as a way to randomly reward move books for excavation sites, battle rewards, etc
* Merchants now sell crafting materials instead of berries
* Merchants also sell health packs which are a single use item that fully heals your party
* The creative mode mob spawner interface was changed to have more monster slots
* The creative mode mob spawner now allows setting both the width and height of the spawning area
* Battle music now fades in and out at the start and end of a battle
* Updated MineClone node groups and biome groups
* Added item group support for Exile
* Added a Zoonami crafting page when playing on Exile
* A few more biome mods, such as Everness, are now supported
* Improved item textures for books, repellent, sanded plank, and NPC spawn eggs
* Optimized the mob spawn step function
* Removed door protection from classic door
* Added mystery egg node protection check
* Glass walk sound was changed
* Adjusted rug crafting recipes to use cloth instead of wool

Bug Fixes
* Updated mesecons.lua to not allow pushing new nodes, like mob spawner
* Added crafting recipe for sticks to support games that don't use group:stick
* Minor fixes/changes to some of the monster sprites
* Adjusted NPC spawns to reduce spawning on roofs
* Adjusted Freazel, Fuzall, and Burrlock collision box sizes to allow walking up snow covered nodes
* Adjusted Merin and Spyder to spawn on grass and sand instead of crumbly due to snow being crumbly
* Fixed NPC spawn eggs adding a prisma ID if carrying a prism
* Fixed crash when trying to use jelly if party and folder 1 box 1 are full
* Fixed lighting for mesh nodes
* Fixed deserializing nil in backpack PVP battle rules
* Fixed mob spawn step from stopping for all players if one spawn fails
* Fixed bug where battle error messages, such as not enough energy to use a move, would conflict with the PVP turn timer if triggered right before the PVP turn timer ran out
* Fixed showcasing prisma monsters using the non-prisma textures
* Fixed the monster indicator in not working in trainer/PVP battles
* Fixed improved jelly and advanced jelly not being removed from the player's inventory when used during battle
* Fixed search sound in monster journal and move journal
* Fixed whirlwind move animation

## Version 0.15.0
* 4 new monsters
* 5 new moves
* New move type (Blitz Moves)
* Damage calculation overhauled
* Move powers adjusted for the new damage calculation
* Same type attack bonus increased from 110% to 115% for the new damage calculation
* Added a backpack setting to allow players to enable a chat bar during battle
* Added a configurable monster spawner block for creative mode players
* Added 4 new pictures
* Pictures are now craftable
* CHANGELOG.md file containing all of the changelogs was added
* Fixed the NPC Chair being uncraftable and changed the crafting recipe
* Fixed the backpack monster rename button and rename field not scaling with the rest of the formspec

## Version 0.14.1
* Increased automatic vending machine item quantity and adjusted prices
* Added crafting recipe for lights
* Added crafting recipe to make countertop drawers back into countertop (crafting loop)
* Fixed mob life timer bug
* Fixed 5th monster slot in backpack being cut off at the bottom

## Version 0.14.0
* 7 new monsters
* 6 new moves (2 new move types)
* Move books for the new moves
* New move types were added to the guide book
* Updated move pools for monsters
* AI can use recover and counter moves
* 3 new battle backgrounds
* Prisma monsters
* Added creative mode item to change spawn egg mobs to prisma
* Monster nicknames
* Setting to disable mob collisions
* Overhauled biome compatibility
* Adjusted berry bush and flower fill ratios if biome table is empty
* NPC chairs now require a minimum natural light level of 1
* ZC can be withdrawn from bank as an item
* Adjusted items with right click GUIs to only open when not right clicking something else
* Taming and trading events are now logged
* All right click, on place, and on secondary functions now check for a valid player
* Excavation sites have golden jelly as a rare drop
* NPC merchants can sell crystal glass
* Crystal lights drop crystal blocks when broken
* Fixed a few minor color palette issues with monsters
* Fixed max nearby NPCs setting (previously had no effect resulting in a bunch of NPCs)
* Small adjustment to cliff detection and Merin walk speed
* Fixed Infobox NPCs from despawning
* Fixed special characters in user input in formspecs
* Fixed Zeenite ore being considered as not ground content which resulted in floating ore
* Fixed minor issues with NPC skins
* Compress PNGs by roughly 18% to save 36 Kibibytes
* Added EXP multiplier player config setting
* Added min and max values for some of the settings
* Changed non-fixed sized formspecs to use relative size fonts
* Reorganized backpack code
* Decreased berry bush growth speed
* Added Brontore to mystery egg
* Adjusted positioning of 2D sprites for Dromite, Kackaburr, Goopo, Muddle, Shelkern, and Spyder

## Version 0.13.0
* Guide book sections have been overhauled
* Added chat command for players to view the PVP stats of other players
* Trainer dialogue is different if player has not chosen a starter, has no monsters, or monsters are 0 hp
* Changed "EXP Base" to say "EXP Per Level" in monster journal for clarity
* Added a "set prices" button to player vending machines for clarity
* Optimized Zoonami OBJs (14580 bytes saved * 18.15% decrease)
* Fixed formspec zoom variable scope conflicting with other players
* Fixed crystal and excavation site spawning
* Fixed setting vending machine prices
* Fixed items inventory list sometimes not existing causing debug messages
* Fixed backpack showcase monster function not checking if "monster" is a valid monster which could lead to crashes

## Version 0.12.0
* Added PVP battles
* Added PVP chapter to guide book

## Version 0.11.0
* 3 new monsters
* Merchant NPCs
* Excavation sites generate underground
* Added a page to the getting started section in the guide book explaining excavation sites
* Added mystery eggs as possible loot from excavation sites
* Rampede and Ruptore spawning rarity changed due to mystery eggs added
* Greatly improved village generation
* Added flower bed and water fountain structures to villages
* NPC spawning in villages was adjusted to give NPCs priority over monsters
* Villages can now spawn in cold desert biome
* Added prepopulate villages multiplier and max nearby npcs settings
* Added pail to move liquids that Zoonami adds
* Computer battle AI can now use static moves
* Display amount of monsters opponent has for PVP and trainer battles
* Trading machine formspec has a better looking blue theme
* Classic door windows are now transparent
* Added crafting recipe for classic door
* Improved NPC stay near movement
* Showcase monsters are now immediately removed when the owner isn't detected nearby
* The computer battle AI move selection was slightly improved
* Berry bushes now use node timers instead of ABMs
* Fixed missing fallback glass dug sound
* Fixed switch in sound effect still playing after exiting a battle
* Adjusted battle background detection for caves
* Reduced Merin mob walk speed
* Adjusted monster rewards
* Fixed battle reward code to drop items if player inventory is full
* Fixed lava swimming mobs
* Fixed deprecated metadata handling in guide books

## Version 0.10.1
* Fixed item leftovers when player inventory is full for vending machines and berry bushes
* Fixed the missing move book items for the taught moves "diced" and "monochrome"
* Fixed monster repellent from working on showcase monsters
* Fixed a few typos in the license.txt file

## Version 0.10.0
Big Changes
* 7 new monsters were added bringing the total number of monsters in Zoonami up to 30
* 16 new moves were added bringing the total number of moves in Zoonami up to 73
* A trading machine has been added allowing players to trade with other players
* Added a move journal that is like an encyclopedia for all moves in Zoonami
* Added a monster journal that is like an encyclopedia for all monsters in Zoonami
* Added a progression system to the monster journal to only show monsters a player has tamed or traded for
* Added a setting to toggle the monster journal progression system on or off
* Monster/trainer battle AI has been completely overhauled
* Trainer battle AI can now switch monsters
* Trainer NPCs can now have more than one monsters
* Trainer NPCs now use a 5 star difficulty system to let players know how strong the NPC monsters are before starting a battle
* Trainer battles now give 2.5 times more ZC than wild monster battles
* NPC spawning has been overhauled to use NPC chairs allowing players to build villages as long as conditions are met for the NPC chairs
* NPC chairs have been added to mapgen house schematics
* Added a new infobox NPC type that allows players with creative priv to configure a custom chatterbox NPC
* The backpack GUI has been overhauled
* Monsters can learn moves when leveling up
* Monster moves can be remapped in the backpack GUI by clicking on the blue move buttons on the monster page
* Players can showcase a monster in their party by clicking the heart icon on the monsters page for any monster
* Detailed monsters stats can now be viewed in the backpack GUI by clicking on the 3 blue stacked lines icon on the monsters page
* Mob AI has been improved to avoid cliffs and water
* Mobs AI has been improved to allow for swimming, floating, sinking, or dying in water or lava
* Redo how energy is gained when leveling up
* Moves now have a priority system that plays a part in determining which monster attacks first each turn
* The monster sprite is now hidden after dying in battle making it visually easier to understand what is happening
* The battle text boxes are now displayed in front of move animations
* Players are now invincible during a battle to prevent environmental and player damage from interrupting the battle
* Added move books for teaching monsters new moves that can't be learned up leveling up
* Added a section for explaining NPCs in the guide book
* Added a section for explaining basic moves, static moves, and move priority in the guide book
* Added a section for explaining the monster journal and move journal in the guide book
* Added a training section to the guide book
* Added a monster repellent item that automatically despawns a monster on left click
* Removed active mob limit setting because it very poorly implemented and not functioning correctly (will hopefully be added in again after working out issues)

Small Changes
* The battle button sound effects no longer play when selecting a move as to no overlap the move sound
* Added sound effects for switching monster and skip in battle
* Adjusted the grassland battle background grass color to be greener
* Added some new settings for controlling mapgen features
* Renamed some existing settings to better clarify what they do
* Lowered max_nearby_mobs setting default value
* Increased the EXP earned from battles
* Villages can now spawn in glacier, desert, prairie, sandstone, and sandclay biomes
* Villages can now spawn on stone and crumbly nodes
* Mapgen features now make use of the "biomes.lua" functions to increase compatibility with games and biome mods
* Adjusted automatic vending machine prices
* Prevent accidental block placement when picking berries
* Removed "buildable_to" group for berry bushes
* Decreased berry bush selection box size
* Reworded readme file and fixed spelling error
* Reworded a few parts in the guide book

Bug Fixes
* Fixed Zeenite Ore not generating
* Prevent Mesecons pistons pushing healers, computers, trading machines, vending machines, bushes, and other nodes that shouldn't be movable
* Battle rewards are now only given if all of a trainers monsters are defeated
* Fixed enemy bubble animation
* Fixed enemy poison sting move animation
* Non-player entities can no longer right click on Zoonami nodes that have formspecs
* Fixed starter monsters being able to be deleted and traded
* Fixed a bug that allowed cheaters to use items in trainer and PVP battles
* Fixed the mob despawn timer to calculate the correct time
* Implemented a temporary fix for setting vending machine stack prices until a more proper solution is made
* Fixed static save not being set for mobs
* Fixed a few monster colors in the battle party menu being incorrect colors
* Fixed some issues when a player switched monsters in battle
* Custom entity properties are now properly prefixed with an underscore
* Fixed water monsters sometimes swimming on land due to feet position being calculated wrong
* Fixed NPC "talking to" behavior sometimes not getting cleared correctly
* Fixed scroll events in backpack and battle items menu from causing "exploit attempt" debug messages
* The backpack items page now adjusts to different inventory sizes instead of assuming the default size
* Fixed music still playing if player leaves during the battle intro
* Fixed the transfer-zc chat command order
* Fixed then energy cost for Thissle Missle and Mudslide moves
* Trainer NPC monsters are now healed and have energy restored before starting a battle
* Fixed node texture for wood table
* Removed NPC debug chat message that was accidentally left in

## Version 0.9.5
* Mineclone 2 node sounds are used if detected
* Mineclone 2, Farlands Reloaded, and Hades Revisted should now be playable
* Fixed monsters and NPCs not spawning properly during mapgen by switching to an LBM

## Version 0.9.4
* Fix crash when talking to an NPC

## Version 0.9.3
* Improve biome battle background consistency with environment
* Added optional dependency on default to properly use sound from it if it's installed or fallback to sounds in Zoonami
* Converted the Zoonami fallback wood footstep sound to mono so it can be play positionally
* Added missing Shellephant front texture
* Added back the delay before spawning generated NPCs in villages

## Version 0.9.2
* Added 3 new monsters
* Fixed monster mapgen spawning
* Added setting to control monster mapgen fill ratio
* Removed cools_lava as spawn group as it counts as water and ice. This fixes Fuzall from spawning on water instead of snow.
* Adjusted NPCs spawning/generating too many nearby in villages
* Fixed using default stone texture without depending on default
* Added 3D mobs mod as an optional dependancy

## Version 0.9.1
* Fixed swimming mobs and floating mobs from coming up out of the water
* Fixed spawn eggs spawning monsters at wrong height
* Mobs now have a life timer of 30 minutes
* Life timer no longer decreases with a mob is unloaded
* Monsters generate with new chunks during mapgen
* Spawn interval decreased from 10 to 7
* Removed no spawn range around players as it was implemented wrong, misleading, and uneccessary
* No longer require a certain amount of spawning spaces for a wild monster to spawn
* Slightly decreased max nearby NPCs when they generate in villages
* Overhauled the nodes that monsters spawn on and spawn by to allow for more consistent spawns
* Fixed nearby mobs detection when spawning a mob from using the wrong position and resulting in very few wild spawns

## Version 0.9.0
* Added a computer to store monsters
* Made a guide book to explain how to play Zoonami
* Created separate 3D mobs mod
* Added 13 new moves
* NPCs stand still during talking and battling
* Increased starter monster level from 8 to 10
* Generate monsters with moves based on required level
* Prevent items in trainer battles
* Switch to 9 slice buttons for most formspecs
* Added zeenite ore
* Added bookshelf recipe
* Added crystal glass
* Windows are crafted from crystal glass
* Increased NPC wild spawn rarity
* Fixed battle field_item security flaw
* Fixed NPC mob data not being saved
* Battle sound effects use ephemeral boolean
* Adjusted move sounds and animations
* Minor adjustments to Chatterbox NPC messages
* Adjusted mob spawning y position to change based on mob collision box
* Remove one spawn egg when used unless in creative
* Make vending machine owner formspec use dynamic size
* Players with protection bypass can now break player vending machines even if they have items
* Move vending machine code to separate file
* Fix Buderfli, Foliedge, Muddle, and Bubblebee from going over color limit
* Don't change lifetimer for mobs that don't despawn

## Version 0.8.1
* Attempt to fix a crash with default mod not being properly detected when choosing to use node sounds from it or to use fallback node sounds.
* Added missing node fallback sounds that were accidentally removed at some point.

## Version 0.8.0
* Overhauled mob spawning code to handle a large amount of mobs
* Overhauled mob AI to reduce CPU usage
* Mobs Redo no longer a dependency
* Reduced number of NPCs that generate in large villages to an appropriate amount

## Version 0.7.1
* Fixed typo in Fuzall monster stats

## Version 0.7.0
* Added Zoonami Coins
* Zoonami Coins earned from wild battles
* Transfer Zoonami Coins with the transfer-zc command
* Added automatic vending machines to shops
* Added player controlled vending machines
* 8 new monsters
* 5 new moves
* Doors support protection
* Berry bushes support protection
* Add chat message when using healer
* Give backpack to respawning players
* Fix both door states showing in creative mode
* Fix backpack sound error
* Fixed door deleting nodes above it
* Battle rewards text color changed to light gray/white

## Version 0.6.3
* Added missing move animation textures

## Version 0.6.2
* Improved village spawning
* NPCs generate with villages
* Village spawning is adjusted to be lower for "flat" and "carpathian" mapgen
* Other minor changes, such as healer description being changed to "Zoonami Healer" for clarity

## Version 0.6.1
* Fixed berry bush item drops

## Version 0.6.0
* New flowers
* Enviromental crystal puzzles in caves
* Proper healer machine
* Villages
* NPCs
* The backpack has a crafting recipe
* Item rewards after
* Chat command for monster stats outputs to a formspec instead of chat
* Adjusted monster stepheight
* Fixed missing monster glow
* Lowered scorch move volume
* Make a gui background to replace default mod background
* Add missing node fallback sounds if default mod is missing

## Version 0.5.1
* Wild monsters can now be tamed using Jelly.
* Jelly can be crafted from a 2x2 of berries. The berries must be the same type.
* Orange and green berries were added in addition to the blue and red berries.
* One new monster was added.
* Used string patterns to help with handling backpack and battle fields.
* Battle music for trainer battles has been added. (Trainers don't exist yet.)
* Change sound effects to be empheral. This should help reduce stutter when playing sound effects.
* The bonus when using a move type the same as the monster type who is using it has been fixed. It was being calculated incorrectly resulting in higher damage.

## Version 0.5.0
* Added monster statistics on monster page in backpack.
* Added chat command to view advanced monster stats of monsters in party slots. For example, "/monsterstats 1". The command outputs 15 lines of stats, so it is recommended to either increase the amount of chat messages in your Minetest settings or understand that you will need to open the chat and scroll to see all of the info. This may change in the future if I can find a better way to show the info or condense it.
* Two new monsters were added.
* One new move was added.
* Battle music and SFX volume controls in backpack. Due to how Minetest handles volume gain on sounds played (can't go above 100%), the SFX max volume and music max volumes are different. The SFX max volume only goes to 100% because most sounds already play at 100%. However, the music volume is set lower, so the max volume can be set to 200% to make it play at it's original volume.
* Effective move bonus reduced from 1.5 (150%) to 1.25 (125%).
* Move type bonus added. Increases attack bonus by 1.1 (110%) if the move type is the same as the monster type who using the move.
* Minimum attack damage decreased to match decreased max stats in last update.
* Increase level up sound effect volume.
* Player metadata is handled better now in a way that new features can be added to existing worlds.
* Backpack formspec is now displayed using the fsc mod like the battle formspec is making it more secure.
* Empty monster slots are shown in battle party menu.
* Monster type effectiveness lua code was reduced to a proper 2D table/array. Easier to read and update.
* Fixed Zoonami Healer crash when using on_secondary_use.
* Fixed energy not recovering for alive monster if other monster can no longer battle that turn.
* Fixed glitch with empty move slots during battle not being added correctly.
* Fixed handling malicious input from empty and nil monster slots during battle.

## Version 0.4.2
* Battle music has been updated.
* Added 13 new moves.
* Added 4 new monsters.
* The 2D mobs that roam the world now use the front and back sprites.
* SFINV is no longer used. The backpack formspec is only accessible via the backpack. This was necessary as the backpack needs the fonts to scale with the rest of the GUI. Without it, displaying monster statistics and the settings page would be impractical to work on all devices.
* Added an option to scale the backpack formspec in the backpack settings page.
* Starter monsters are now level 8 instead of 10.
* Starter monsters have proper moves instead of hard coded moves used for testing.
* Slowed down the battle intro animation slightly.
* More EXP is given from all battles.
* Jump height was increased for 2D mobs so they can navigate the terrain better.
* Mob spawning conditions are more advanced. For example, Maluga spawns in water. Scallapod spawns on sand that is near water.
* Fixed a bug where players could switch to an already active monster.
* Fixed a bug that caused move priority based on agility to be wrong.
* Fixed battle intro animation frame positioning.
* Fixed bug with move slots not being filled correctly in battle formspec.

## Version 0.4.1
* The party and item battle menus have a new background that matches the Zoonami backpack background
* Adjust backpack formspec to look and function better on smaller screens
* Added 7 new moves
* Give Zoonami Backpack item to all new players
* New players can choose their starter monster from the backpack monster menu page. After it's chosen, the page will be used to show the monsters in your party, but that's not implemented yet so it will appear blank.
* EXP is given to the monster that defeats another monster. It's not split with any other monsters.
* Handles formspec fields better to separate "numbered lists" for items, monsters, etc
* Monsters can level up and the stats go up accordingly (learning new moves is not implemented yet)

## Version 0.4.0
* Switched to notabug.org
* Added missing button textures for backpack that I forgot to include in 0.3.1
* Taking advantage of new feature added in August in 5.4.0 to organize textures into subfolders
* Organized battle code and condensed it by 31 lines
* Made starting a battle with wild monsters only start if the entity right clicking it has a monster with 1 or more health.
* The first monster that a player starts the battle with can no longer be one with 0 health. It will continue down the player's party until it finds the first one with 1 or more health.
* Handles formspec fields better to separate "numbered lists" for items, monsters, etc
* Changed the monster move selection menu buttons to be more space effecient allowing for longer move names
* Added blank menu buttons if a monster knows less than 4 moves to fill the empty space for aesthetic reasons
* Decided on 13 monster/move types and created their strengths and weaknesses
* Added the moves Prickle, Spore Storm, Thissle Missle, Swipe, Gnaw, Bite, Claw, Pincer, and Bubble Stream
* Added a message about move effectiveness when using a move; it will be changed as it's hard to read and see the damage and animation in only 3 seconds, but it's better than nothing.

## Version 0.3.1
* This small update is the first step towards how items will be found in the world and used in battle. Berry bushes will be uncommonly found around the world. Right clicking on them will harvest the berries. Place the berries in your Zoonami backpack menu under the "Items" section. Items in the Zoonami backpack are accessible during battles if the items have a functionality that can be used. Currently Blue Berries and Red Berries give +10 Health. They will most likely change to do something else in the future.

## Version 0.3.0
* This update introduces the backpack menu. The backpack menu allows access to items, monsters, settings, and player stats. The menu can be accessed either via the menu tab if SFINV is installed or right clicking while holding the backpack item. Currently there is no crafting recipe for the backpack. It will be added in a later update.

## Version 0.2.0
* This release introduces switching after a player's monster faints in battle and different battle backgrounds depending on the biome name or nodes that the player is standing on.

## Version 0.1.2
* Fixes a bug introduced in v0.1.1 that crashes the game when switching monsters.

## Version 0.1.1
* Created formspec functions to make formspec code easier to read and easier to implement GUI scaling.
* Fixed a crash if the computer player used "skip".
* Merged menu, animation, and textbox variables with battle.context.
* Segment battle.update for readability.
* Merged "skip" to be a move that way the code can consistently utilize the "type" for moves, monsters, and items in the future.

## Version 0.1.0
* Initial release of tech demo.